#pragma once
#include <iostream>
#include <algorithm>
#include "Queue.h"

template <class T>
class BinarySearchTree
{
private:
	template <class T>
	struct Node {
		T key_;
		Node<T> *left_;
		Node<T> *right_;
		Node<T> *p_;
		Node(T key, Node *left = nullptr, Node *right = nullptr, Node *p = nullptr) :
			key_(key), left_(left), right_(right), p_(p)
		{ }
	};

public:
	BinarySearchTree();
	BinarySearchTree(const BinarySearchTree<T> & src) = delete;
	BinarySearchTree(BinarySearchTree<T>&& src) noexcept;
	BinarySearchTree<T>& operator= (const BinarySearchTree<T>& src) = delete;
	BinarySearchTree<T>& operator= (BinarySearchTree<T>&& src) noexcept;
	virtual ~BinarySearchTree();

	void treatment(Node<T>* node) const;

	// 1.1 ������� ������ �� ����� � �������� ������ ������
	bool iterativeSearch(const T& key) const;

	// 2 ������� ������ �������� � ������: true, ���� ������� ��������; 
	// false, ���� ������� ��� ���
	bool insert(const T& key);

	// 3.1 �������� �������� �� ������, �� ���������� ������� ���������
	// true, ���� ������� ������; false, ���� �������� �� ����
	bool deleteKey(const T& key);

	// 4.1 ������ ���������� ����������� ������ � �������� ����� out,
	// ������������ ������, ����� �������� ��������� ������
	void print(std::ostream& out) const;

	// 5.1 ����������� ���������� ����� ������
	size_t getCount() const;

	// 6.1 ����������� ������ ������
	size_t getHeight() const;

	// 7 ��������� ����� ������ (�����������) 
	void iterativeInorderWalk() const;

	// 8.1 ��������� ����� ������ (�����������) 
	void inorderWalk() const;

	// 9 ����� ��������� ������ �� ������� (� ������). 
	void walkByLevels() const;

	// 10 �������� �� ��� ������ ��������
	bool isSimilar(const BinarySearchTree<T> & other) const;

	// 11 ���� ���������� ����� � ���� �������� ������
	bool isIdenticalKey(const BinarySearchTree<T> & other) const;

private:
	// 1.2 ������� ������ ������ ���� �� ����� � �������� ������ ������
	Node<T>* iterativeSearchNode(const T& key) const;

	Node<T>* minimumSearch(Node<T>* node) const;
	Node<T>* maximumSearch(Node<T>* node) const;
	Node<T>* successorIterativeSearch(Node<T>* node) const;

	// 4.2 ����������� ������� ��� ������ ����������� ������ � �������� �����
	void printNode(std::ostream& out, Node<T>* root) const;

	// 5.2 ����������� ������� ����������� ���������� ����� ������
	size_t getCount(const Node<T>* node) const;

	// 6.2 ����������� ������� ����������� ������ ������
	size_t getHeight(const Node<T>* node) const;

	// 8.2 ����������� ������� ��� ���������� ������ ����� ������.
	void inorderWalk(Node<T>* node) const;

	void deleteTree(Node<T>* node);

	Node<T> *root_;
};

template <class T>
BinarySearchTree<T>::BinarySearchTree()
	: root_(nullptr)
{

}

template <class T>
BinarySearchTree<T>::BinarySearchTree(BinarySearchTree<T>&& src) noexcept
{
	this->root_ = src->root_;
	src->root_ = nullptr;
}

template <class T>
BinarySearchTree<T>& BinarySearchTree<T>::operator= (BinarySearchTree<T>&& src) noexcept
{
	this->root_ = src->root_;
	src->root_ = nullptr;
	return this;
}

template <class T>
BinarySearchTree<T>::~BinarySearchTree()
{
	deleteTree(root_);
}

template <class T>
void BinarySearchTree<T>::treatment(Node<T>* node) const
{
	// Processing for the node
}

template <class T>
bool BinarySearchTree<T>::iterativeSearch(const T& key) const
{
	return (iterativeSearchNode(key) != nullptr);
}

template <class T>
bool BinarySearchTree<T>::insert(const T& key)
{
	Node<T>* node = new Node<T>(key);
	if (root_ == nullptr) {
		root_ = node;
		return true;
	}
	Node<T>* prev = nullptr;
	Node<T>* temp = root_;
	while (temp != nullptr) {
		if (temp->key_ > key) {
			prev = temp;
			temp = temp->left_;
		}
		else if (temp->key_ < key) {
			prev = temp;
			temp = temp->right_;
		}
		else {
			return false;
		}
	}
	node->p_ = prev;
	if (prev->key_ > key) {
		prev->left_ = node;
	}
	else {
		prev->right_ = node;
	}
	return true;
}

template <class T>
bool BinarySearchTree<T>::deleteKey(const T& key)
{
	Node<T>* x = root_;
	Node<T>* p = nullptr;
	while (x != nullptr && x->key_ != key)
	{
		p = x;
		if (x->key_ > key)
		{
			x = x->left_;
		}
		else
		{
			x = x->right_;
		}
	}
	if (x == nullptr) {
		return false;
	}
	if (x->left_ == nullptr && x->right_ == nullptr) {
		if (x->p_->left_ == x) {
			x->p_->left_ = nullptr;
			delete x;
			return true;
		}
		if (x->p_->right_ == x) {
			x->p_->right_ = nullptr;
			delete x;
			return true;
		}
	}
	if (x->left_ == nullptr && x->right_ != nullptr) {
		if (x->p_ == nullptr) {
			root_ = x->right_;
			x->right_->p_ = nullptr;
			delete x;
			return true;
		}
		if (x->p_->left_ == x) {
			x->p_->left_ = x->right_;
			x->right_->p_ = x->p_;
			delete x;
			return true;
		}
		x->p_->right_ = x->right_;
		x->right_->p_ = x->p_;
		delete x;
		return true;
	}
	if (x->left_ != nullptr && x->right_ == nullptr) {
		if (x->p_ == nullptr) {
			root_ = x->left_;
			x->left_->p_ = nullptr;
			delete x;
			return true;
		}
		if (x->p_->right_ == x) {
			x->p_->right_ = x->left_;
			x->left_->p_ = x->p_;
			delete x;
			return true;
		}
		x->p_->left_ = x->left_;
		x->left_->p_ = x->p_;
		delete x;
		return true;
	}
	Node<T>* y = x->left_;
	Node<T>* replace = maximumSearch(y);
	x->key_ = replace->key_;
	if (replace->left_ == nullptr) {
		if (replace->p_ != x) {
			replace->p_->right_ = nullptr;
		}
		else {
			x->left_ = nullptr;
		}
	}
	else {
		replace->p_->right_ = replace->left_;
		replace->left_->p_ = replace->p_;
	}
	delete replace;
	return true;
}

template <class T>
void BinarySearchTree<T>::print(std::ostream& out) const
{
	printNode(out, root_);
}

template <class T>
size_t BinarySearchTree<T>::getCount() const
{
	return getCount(this->root_);
}

template <class T>
size_t BinarySearchTree<T>::getHeight() const
{
	return getHeight(this->root_);
}

template <class T>
void BinarySearchTree<T>::iterativeInorderWalk() const
{
	Node<T>* x = root_;
	Node<T>* y = minimumSearch(x);
	while (y != nullptr) {
		treatment(y);
		y = successorIterativeSearch(y);
	}
}

template <class T>
void BinarySearchTree<T>::inorderWalk() const
{
	inorderWalk(root_);
}

template <class T>
void BinarySearchTree<T>::walkByLevels() const
{
	if (root_ == nullptr) {
		return;
	}
	QueueArray<Node<T>*> queue(10);
	Node<T>* x = root_;
	do {
		treatment(x);
		try {
			if (x->left_ != nullptr) {
				queue.enQueue(x->left_);
			}
			if (x->right_ != nullptr) {
				queue.enQueue(x->right_);
			}
			if (!queue.isEmpty()) {
				x = queue.deQueue();
			}
		}
		catch (const QueueArray<Node<T>*>::QueueUnderflow& e) {
			std::cerr << "Exception: " << e.getMessage() << std::endl;
			return;
		}
		catch (const QueueArray<Node<T>*>::QueueOverflow& e) {
			std::cerr << "Exception: " << e.getMessage() << std::endl;
			return;
		}
	} while (!queue.isEmpty() || (x->left_ != nullptr || x->right_ != nullptr));
	treatment(x);
}

template <class T>
bool BinarySearchTree<T>::isSimilar(const BinarySearchTree<T> & other) const
{
	Node<T>* node1 = minimumSearch(this->root_);
	Node<T>* node2 = minimumSearch(other.root_);
	if (node1 == nullptr) {
		if (node2 == nullptr) {
			return true;
		}
		return false;
	}
	else if (node2 == nullptr) {
		return false;
	}
	if (node1->key_ == node2->key_) {
		while (node1 != nullptr && node2 != nullptr) {
			node1 = successorIterativeSearch(node1);
			node2 = successorIterativeSearch(node2);
			if (node1 != nullptr && node2 != nullptr) {
				if (node1->key_ != node2->key_) {
					return false;
				}
			}
		}
		if (node1 == nullptr && node2 == nullptr) {
			return true;
		}
	}
	return false;
}

template <class T>
bool BinarySearchTree<T>::isIdenticalKey(const BinarySearchTree<T> & other) const
{
	Node<T>* node1 = minimumSearch(this->root_);
	Node<T>* node2 = minimumSearch(other.root_);
	if (node1 == nullptr) {
		if (node2 == nullptr) {
			return true;
		}
		return false;
	}
	else if (node2 == nullptr) {
		return false;
	}
	if (node1->key_ != node2->key_) {
		while (node1 != nullptr && node2 != nullptr) {
			if (node1->key_ < node2->key_) {
				node1 = successorIterativeSearch(node1);
			}
			else {
				node2 = successorIterativeSearch(node2);
			}
			if (node1 != nullptr && node2 != nullptr) {
				if (node1->key_ == node2->key_) {
					return true;
				}
			}
		}
		return false;
	}
	return true;
}

template <class T>
BinarySearchTree<T>::Node<T>* BinarySearchTree<T>::iterativeSearchNode(const T& key) const
{
	Node<T>* node = root_;
	while (node != nullptr) {
		if (key < node->key_) {
			node = node->left_;
		}
		else if (key > node->key_) {
			node = node->right_;
		}
		else {
			return node;
		}
	}
	return nullptr;
}

template <class T>
BinarySearchTree<T>::Node<T>* BinarySearchTree<T>::minimumSearch(Node<T>* node) const
{
	if (node == nullptr) {
		return nullptr;
	}
	while (node->left_ != nullptr) {
		node = node->left_;
	}
	return node;
}

template <class T>
BinarySearchTree<T>::Node<T>* BinarySearchTree<T>::maximumSearch(Node<T>* node) const
{
	if (node == nullptr) {
		return nullptr;
	}
	while (node->right_ != nullptr) {
		node = node->right_;
	}
	return node;
}

template <class T>
BinarySearchTree<T>::Node<T>* BinarySearchTree<T>::successorIterativeSearch(Node<T>* node) const
{
	if (node == nullptr) {
		return nullptr;
	}
	if (node->right_ != nullptr) {
		return minimumSearch(node->right_);
	}
	Node<T>* parent = node->p_;
	while (parent != nullptr && node == parent->right_) {
		node = parent;
		parent = parent->p_;
	}
	return parent;
}

template <class T>
void BinarySearchTree<T>::printNode(std::ostream& out, Node<T>* root) const
{
	if (root == nullptr) {
		return;
	}
	out << root->key_;
	if (root->left_ == nullptr && root->right_ == nullptr) {
		return;
	}
	out << '(';
	printNode(out, root->left_);
	out << ')';
	if (root->right_ != nullptr) {
		out << '(';
		printNode(out, root->right_);
		out << ')';
	}
}

template <class T>
size_t BinarySearchTree<T>::getCount(const Node<T>* node) const
{
	if (node == nullptr) {
		return 0;
	}
	return (1 + getCount(node->left_) + getCount(node->right_));
}

template <class T>
size_t BinarySearchTree<T>::getHeight(const Node<T>* node) const
{
	if (node == nullptr) {
		return 0;
	}
	return (1 + std::max(getCount(node->left_), getCount(node->right_)));
}

template <class T>
void BinarySearchTree<T>::inorderWalk(Node<T>* node) const
{
	if (node != nullptr) {
		inorderWalk(node->left_);
		treatment(node);
		inorderWalk(node->right_);
	}
}

template <class T>
void BinarySearchTree<T>::deleteTree(Node<T>* node)
{
	if (node == nullptr) {
		return;
	}
	deleteTree(node->left_);
	deleteTree(node->right_);
	delete node;
}