#pragma once
#include <string>

template <class T>
class Queue
{
public:
	class QueueException
	{
	public:
		QueueException(const std::string& message);
		std::string getMessage() const;
	private:
		std::string exceptionMessage;
	};

	virtual ~Queue() {}
	virtual void enQueue(const T& e) = 0;
	virtual T deQueue() = 0;
	virtual bool isEmpty() = 0;
};template <class T>
class QueueArray : public Queue<T>
{
public:
	class QueueOverflow : public Queue<T>::QueueException
	{
	public:
		QueueOverflow();
	};

	class QueueUnderflow : public Queue<T>::QueueException
	{
	public:
		QueueUnderflow();
	};

	class WrongQueueSize : public Queue<T>::QueueException
	{
	public:
		WrongQueueSize();
	};

	QueueArray(size_t size = 100);
	~QueueArray();
	virtual void enQueue(const T& e) override;
	virtual T deQueue() override;
	virtual bool isEmpty() override;
	bool isFull() const;

private:
	QueueArray(const QueueArray<T>& src);
	QueueArray& operator=(const QueueArray<T>& src);
	QueueArray& operator=(QueueArray<T>&& src);
	size_t length;
	T* que;
	size_t beginQue;
	size_t endQue;
};template <class T>
Queue<T>::QueueException::QueueException(const std::string& message)
	: exceptionMessage(message)
{}

template <class T>
std::string Queue<T>::QueueException::getMessage() const
{
	return exceptionMessage;
}

template <class T>
QueueArray<T>::QueueOverflow::QueueOverflow()
	: Queue<T>::QueueException("Queue Overflow")
{}

template <class T>
QueueArray<T>::QueueUnderflow::QueueUnderflow()
	: Queue<T>::QueueException("Queue Underflow")
{}

template <class T>
QueueArray<T>::WrongQueueSize::WrongQueueSize()
	: Queue<T>::QueueException("Wrong Queue Size")
{}
template <class T>
QueueArray<T>::QueueArray(size_t size)
{
	if (size == 0) {
		throw WrongQueueSize();
	}
	else {
		try {
			que = new T[size];
			length = size;
			beginQue = 0;
			endQue = 0;
		}
		catch (std::bad_alloc) {
			throw WrongQueueSize();
		}
	}
}

template <class T>
QueueArray<T>::~QueueArray()
{
	if (que != nullptr) {
		delete[] que;
	}
}

template <class T>
void QueueArray<T>::enQueue(const T& e)
{
	if (!isFull()) {
		que[endQue] = e;
		endQue = (endQue + 1) % length;
	}
	else {
		throw QueueOverflow();
	}

}

template <class T>
T QueueArray<T>::deQueue()
{
	if (!isEmpty()) {
		T curr_elem = que[beginQue];
		beginQue = (beginQue + 1) % length;
		return curr_elem;
	}
	else {
		throw QueueUnderflow();
	}
	return 0;
}

template <class T>
bool QueueArray<T>::isEmpty()
{
	if (beginQue == endQue) return true;
	else return false;
}template <class T>
bool QueueArray<T>::isFull() const
{
	if ((beginQue - endQue + length) % length == 1) return true;
	else return false;
}