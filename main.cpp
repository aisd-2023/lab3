﻿#include <iostream>
#include "BinarySearchTree.h"

void createTree(BinarySearchTree<int>& tree);
void generateTree(size_t count, BinarySearchTree<int>& tree);


int main()
{
	BinarySearchTree<int> tree;
	createTree(tree);
	std::cout << "Created tree 1:" << std::endl;
	tree.print(std::cout);
	std::cout << std::endl;
	tree.deleteKey(4);
	std::cout << "Created tree 1 after deleting 4:" << std::endl;
	tree.print(std::cout);
	std::cout << std::endl;
	tree.deleteKey(6);
	std::cout << "Created tree 1 after deleting 6:" << std::endl;
	tree.print(std::cout);
	std::cout << std::endl;
	tree.deleteKey(7);
	std::cout << "Created tree 1 after deleting 7:" << std::endl;
	tree.print(std::cout);
	std::cout << std::endl;

	std::cout << "25 is found in the tree: ";
	bool res = tree.iterativeSearch(25);
	std::cout << (res ? "true" : "false") << std::endl;
	std::cout << "2 is found in the tree: ";
	res = tree.iterativeSearch(2);
	std::cout << (res ? "true" : "false") << std::endl;

	size_t count = tree.getCount();
	size_t height = tree.getHeight();
	std::cout << "Number of tree nodes: " << count << std::endl;
	std::cout << "Tree height: " << height << std::endl;

	std::cout << "Iterative infix tree traversal: " << std::endl;
	tree.iterativeInorderWalk();
	std::cout << std::endl;
	std::cout << "Recursive infix tree traversal: " << std::endl;
	tree.inorderWalk();
	std::cout << std::endl;
	std::cout << "Traversal of a binary tree by levels: " << std::endl;
	tree.walkByLevels();
	std::cout << std::endl;

	BinarySearchTree<int> tree2;
	createTree(tree2);
	std::cout << "Created tree 2:" << std::endl;
	tree2.print(std::cout);
	std::cout << std::endl;
	bool sim = tree.isSimilar(tree2);
	std::cout << "Tree 1 and tree 2 are similar: " << (sim ? "true" : "false") << std::endl;
	std::cout << "Trees 1 and 2 have identical key: ";
	bool identKey = tree.isIdenticalKey(tree2);
	std::cout << (identKey ? "true" : "false") << std::endl;

	tree2.deleteKey(15);
	std::cout << "Created tree 2 after deleting 15:" << std::endl;
	tree2.print(std::cout);
	std::cout << std::endl;
	sim = tree.isSimilar(tree2);
	std::cout << "Tree 1 and tree 2 are similar: " << (sim ? "true" : "false") << std::endl;

	std::cout << std::endl;
	BinarySearchTree<int> tree3;
	std::cout << "Generated tree 3:" << std::endl;
	generateTree(100, tree3);
	tree3.print(std::cout);
	std::cout << std::endl;
	std::cout << "Iterative infix tree traversal: " << std::endl;
	tree3.iterativeInorderWalk();
	std::cout << std::endl;
	std::cout << "Recursive infix tree traversal: " << std::endl;
	tree3.inorderWalk();
	std::cout << std::endl;
	std::cout << "Traversal of a binary tree by levels: " << std::endl;
	tree3.walkByLevels();
	std::cout << std::endl;

	return 0;
}

void createTree(BinarySearchTree<int>& tree)
{
	tree.insert(7);
	tree.insert(4);
	tree.insert(12);
	tree.insert(25);
	tree.insert(6);
	tree.insert(15);
	tree.insert(12);
}

void generateTree(size_t count, BinarySearchTree<int>& tree)
{
	srand(652562);
	for (size_t i = 0; i < count; i++) {
		tree.insert(rand());
	}
}

void BinarySearchTree<int>::treatment(BinarySearchTree<int>::Node<int>* node) const
{
	std::cout << node->key_ << " ";
}